;;; package --- Summary

;;; Commentary:
;;;; If you're using `dvorak' keyboard layout and also need writing russian in
;;;; the `jcuken' layout, you may find this package helpful!

;;; Code:

(require 'quail)

(quail-define-package
 "cyrillic-dvorak" "Cyrillic" "ЙЦУК" nil
 "ЙЦУКЕН keyboard layout widely used in Russia (ISO 8859-5 encoding)
  in assuming that your default keyboard layout is dvorak"
 nil t t t t nil nil nil nil nil t)

(quail-define-rules
 ("p" ?п)
 ("y" ?ы)
 ("f" ?ф)
 ("g" ?г)
 ("c" ?ц)
 ("r" ?р)
 ("l" ?л)
 ("a" ?а)
 ("o" ?о)
 ("e" ?е)
 ("u" ?у)
 ("i" ?и)
 ("d" ?д)
 ("h" ?х)
 ("t" ?т)
 ("n" ?н)
 ("s" ?с)
 ("q" ?я)
 ("j" ?й)
 ("k" ?к)
 ("x" ?ш)
 ("b" ?б)
 ("m" ?м)
 ("w" ?в)
 ("v" ?ь)
 ("z" ?з)

 ("P" ?П)
 ("Y" ?Ы)
 ("F" ?Ф)
 ("G" ?Г)
 ("C" ?Ц)
 ("R" ?Р)
 ("L" ?Л)
 ("A" ?А)
 ("O" ?О)
 ("E" ?Е)
 ("U" ?У)
 ("I" ?И)
 ("D" ?Д)
 ("H" ?Х)
 ("T" ?Т)
 ("N" ?Н)
 ("S" ?С)
 ("Q" ?Я)
 ("J" ?Й)
 ("K" ?К)
 ("X" ?Ш)
 ("B" ?Б)
 ("M" ?М)
 ("W" ?В)
 ("V" ?Ь)
 ("Z" ?З))
;;; cyrillic-dvorak.el ends here
