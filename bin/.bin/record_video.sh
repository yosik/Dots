#!/usr/bin/env sh

# Start screen recording and write into file video.mp4
ffmpeg -f x11grab -video_size 1920x1080 -framerate 25 -i $DISPLAY -c:v libx264 -preset ultrafast video.mp4
