#!/usr/bin/env sh

i3lock -n --pass-media-keys \
	   --blur 20 --radius 65 --clock --indicator \
	   --time-color ffffff --inside-color 000000 \
	   --ind-pos 100:900 --time-pos 100:910 --date-pos 250:905 --date-size 25 \
	   --verif-text="" --wrong-text="" --noinput-text="" --lock-text="" --lockfailed-text="" \
	   --time-str="%H:%M" --date-str="Locked" \
	   --date-font="Fira Code" --time-font="Fira Code"
