#!/usr/bin/env raku

sub command_to_string(@command where Positional) {
	return (run @command, :out).out.slurp(:close);
}

my $command = "xinput list";
my $out = command_to_string($command.words).lines;
my $match = $out ~~ /Touchpad \s* id\=(\d+)/;
my $id = $match[0].Str;

run "xinput", "disable", $id;
