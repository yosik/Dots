#!/usr/bin/env raku

use v6;

unit sub MAIN (
	Str  $dir where *.IO.d,  #= An existing directory to unspacify
	Bool :v(:$verbose),      #= Print additional info
	Str  :s(:$symbol) = "_", #= String to replace whitespaces
);

my IO @dirs = $dir.IO;
while @dirs {
	for @dirs.pop.dir -> IO $path {
		next unless $path ~~ /\s/;
		my IO $newpath = $path.subst(/\s/, $symbol, :g).IO;
		say "$path -> $newpath" if $verbose;
		try {
			$path.rename: $newpath, createonly => True;
			CATCH {
				when X::IO::Rename {
					if "y" eq prompt "Collision with $newpath, proceed? [yN] " {
						$path.rename: $newpath, createonly => False;
					} else {
						$newpath = $path;
					}
				}
			}
		}
		@dirs.push: $newpath if $newpath.d;
	}
}
