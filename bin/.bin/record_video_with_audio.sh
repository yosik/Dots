#!/usr/bin/env sh

# Start screen and audio recording and write into file video.mp4
ffmpeg -f x11grab -video_size 1920x1080 -framerate 25 -i $DISPLAY -f pulse -i default -c:v libx264 -preset ultrafast -c:a aac video.mp4
