#!/usr/bin/env guile
-s
!#
;; Script to update yt-dlp archives

(use-modules (ice-9 ftw)
             (ice-9 rdelim)
             (ice-9 match))



(define (run cmd)
  (format #t "~a~%" (string-join cmd))
  (let ((status (apply system* cmd)))
    (unless (zero? status)
      (format (current-error-port)
              "warning: Command ~s failed with status ~a~%"
              cmd status))))

(define (file->words file)
  (string-split
   (with-input-from-file file
     read-string)
   char-set:whitespace))

(define (update path)
  (define (dir-contains? dir name)
    (not (null? (scandir dir (lambda (s) (string=? name s))))))

  (define (updatable-dir? path)
    (dir-contains? path ".yt-dlp-source"))

  (define (update-dir path)
    (format #t "~%Updating ~a~%" path)
    (chdir path)
    (run `("yt-dlp"
           "--download-archive" ".yt-dlp-archive"
           "--embed-subs" "--embed-metadata"
           "--sub-langs" "all,-live_chat"
           ,@(file->words ".yt-dlp-source"))))

  (define (process-path filename statinfo flag)
    (when (and (equal? 'directory flag)
               (updatable-dir? filename))
      (update-dir filename))
    #t)

  (ftw path process-path))



(match (program-arguments)
  ((program-file)
   (format #t "Usage: ~a [directory]...~%~%" program-file)
   (format #t "Update yt-dlp archives in DIRECTORIES recursively~%"))
  ((program-file . dirs)
   (for-each update dirs)))
