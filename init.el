;;; init.el --- Config file -*- lexical-binding: t; -*-

;; Copyright (C) 2021, 2022  Iosilevitch Mihail

;; Author: Iosilevitch Mihail <mihail.iosilevitch@yandex.ru>

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:
;; Just my init.el

;;; Code:

(push (expand-file-name "lisp" user-emacs-directory) load-path)

;;; setup.el configuration
(eval-and-compile
  (require 'autoload)
  (require 'setup)
  (setup-define :face
    (lambda (face spec)
      `(custom-set-faces '(,face ,spec 'now "Customized by `setup'.")))
    :documentation "Customize FACE with SPEC using `custom-set-faces'."
    :debug '(form sexp)
    :repeatable t)
  (setup-define :new-map
    (lambda (name key docstring &rest binds)
      `(progn
         (defvar-keymap ,name :doc ,docstring
                        ,@binds)
         (global-set-key ,key ,name)))
    :documentation "Make keymap with NAME, DOCSTRING and BINDS inside.
Bind this keymap on KEY."
    :ensure '(nil kbd nil nil nil)
    :indent 'defun)
  (setup-define :global--into1
    (lambda (key command)
      `(define-key ,(setup-get 'map) ,key ,command))
    :documentation "Globally bind COMMAND to KEY in current map."
    :debug '(form sexp)
    :ensure '(kbd func)
    :repeatable t)
  (setup-define :global-into
    (lambda (map &rest rest)
      `(:with-map ,map (:global--into1 ,@rest)))
    :documentation "Globally bind commands in MAP.
REST is consists of pairs (not in lisp meaning) of key and command."
    :debug '(symbolp &rest form sexp)
    :indent 1))

(setup package
  (:option package-archive-priorities
           '(("gnu" . 20) ("nongnu" . 15))))

;;; The general config
(setup emacs
  (:new-map user-tools-map (kbd "s-t")
    "Keymap for personal bindings.")
  (:global "C-x C-z" nil
           "C-z" nil
           "C-h" #'backward-delete-char
           "M-h" #'backward-kill-word)
  (:global-into ctl-x-map
    "i" #'package-install)
  (defun my-disable-local-tabs ()
    "Disable `indent-tabs-mode', set `tab-width' to 8."
    (indent-tabs-mode -1)
    (setq-local tab-width 8))
  (put 'narrow-to-region 'disabled nil)
  (put 'scroll-left 'disabled nil)
  (put 'upcase-region 'disabled nil)
  (:option cursor-type '(bar . 3))
  (:option blink-cursor-blinks 0)
  (:option tab-width 8)
  (:option standard-indent 8)
  (:option sentence-end-double-space nil)
  (:option fill-column 72)
  (:option ring-bell-function
           (lambda ()
             (unless (memq this-command
                           '(abort-minibuffers
                             exit-minibuffer
                             keyboard-quit))
               (ding))))
  (:option visible-bell 1)
  (:option tool-bar-mode nil) ; Disable toolbar
  (:option menu-bar-mode nil) ; Disable menubar
  (:option scroll-bar-mode nil) ; Disable scrollbar
  (:option custom-file null-device)
  (:option user-full-name "Mihail Iosilevich")
  (:option use-short-answers t)
  (:option echo-keystrokes 0.01)
  (:option goto-line-history-local t)
  (indent-tabs-mode) ; Indents
  (global-auto-revert-mode))

;;; Small handy tweaks

(setup simple
  (defun my-duplicate-current-line ()
    "Duplicate current line."
    (interactive)
    (save-excursion
      (let ((current-line (thing-at-point 'line)))
        (forward-line 1)
        (insert current-line)))
    (line-move 1))
  (defun my-move-bol-dwim ()
    "DWIM command to move at beginning of line.

Move point to the first non-whitespace character on this line. If
already here, move at beginning of line."
    (interactive)
    (let ((start-point (point)))
      (back-to-indentation)
      (when (= start-point (point))
        (move-beginning-of-line 1))))
  (:global "M-SPC" #'cycle-spacing
           "M-u" #'upcase-dwim
           "M-c" #'capitalize-dwim
           "M-l" #'downcase-dwim
           "M-k" #'kill-whole-line
           "s-d" #'my-duplicate-current-line
           [remap move-beginning-of-line] #'my-move-bol-dwim
           [remap back-to-indentation] #'my-move-bol-dwim)
  (:option kill-do-not-save-duplicates t)
  (:option async-shell-command-buffer 'new-buffer)
  (line-number-mode)
  (column-number-mode)
  (size-indication-mode))

(setup shortdoc
  (:global-into help-map
    "s" #'shortdoc))

(setup help
  (:global-into help-map
    "C-f" #'describe-face
    "C-e" #'pp-macroexpand-last-sexp)
  (:global "C-?" #'help-command)
  (:hook #'visual-line-mode))

(setup info
  ;; Based on https://git.sr.ht/~sokolov/dotfiles/
  (defconst my-info-colors-face-alist
    `((,(rx (or "Command" "Function")) . font-lock-function-name-face)
      (,(rx "Constant") . font-lock-constant-face)
      (,(rx (or (seq (? "User ") "Option") "Variable"))
       . font-lock-variable-name-face)
      (,(rx "Syntax class") . font-lock-preprocessor-face)
      (,(rx (* nonl)) . font-lock-keyword-face)))
  (font-lock-add-keywords
   'Info-mode
   `((,(rx bol " -- " (group (+ (not ":"))) ": "
           (group (+ (or (syntax word)
                         (syntax symbol)
                         (syntax punctuation))))
           (? " " (group (+ nonl)))
           eol)
      (1 font-lock-type-face)
      (2 (assoc-default
          (match-string 1)
          my-info-colors-face-alist
          #'string-match-p))
      (3 font-lock-variable-name-face)
      (,(rx bol (= 10 " ") (* nonl) eol)
       (forward-char) nil (0 font-lock-variable-name-face))))))

(setup repeat
  (repeat-mode))

(setup scratch
  (:global-into user-tools-map
    "s" #'scratch))

(setup view
  (:option view-read-only t))

(setup copyright
  (:option copyright-names-regexp
           (rx (or (seq "Mihail Iosilevi" (? "t") "ch")
                   (seq "Iosilevi" (? "t") "ch Mihail"))))
  (:with-hook before-save-hook
    (:hook #'copyright-update)))

(setup files
  (:option make-backup-files t)
  (:option backup-directory-alist
           `(("." . ,(expand-file-name "saves/" user-emacs-directory))))
  (:option version-control t)
  (:option delete-old-versions t)
  (:option kept-new-versions 6)
  (:option kept-old-versions 2)
  (:option save-abbrevs nil)
  (:option kill-buffer-delete-auto-save-files t))

(setup saveplace
  (save-place-mode))

(setup mule-cdms
  (prefer-coding-system 'utf-8)
  (set-default-coding-systems 'utf-8)
  (set-terminal-coding-system 'utf-8)
  (set-keyboard-coding-system 'utf-8))

(setup vundo
  (:global-into ctl-x-map
    "u" #'vundo))

;;; EXWM
(setup (:require exwm)
  (defun my-start-program (command)
    (interactive (list (read-shell-command "$ ")))
    (start-process-shell-command command nil
                                 command))
  (:option exwm-input-simulation-keys
           '(([?\C-b] . [left])
             ([?\C-б] . [left])
             ([?\M-b] . [C-left])
             ([?\M-б] . [C-left])
             ([?\C-f] . [right])
             ([?\C-ф] . [right])
             ([?\M-f] . [C-right])
             ([?\M-ф] . [C-right])
             ([?\C-p] . [up])
             ([?\C-п] . [up])
             ([?\C-n] . [down])
             ([?\C-н] . [down])
             ([?\C-a] . [home])
             ([?\C-а] . [home])
             ([?\C-e] . [end])
             ([?\C-е] . [end])
             ([?\M-v] . [prior])
             ([?\M-ь] . [prior])
             ([?\C-v] . [next])
             ([?\C-ь] . [next])
             ([?\C-d] . [delete])
             ([?\C-д] . [delete])
             ([?\M-d] . [C-delete])
             ([?\M-д] . [C-delete])
             ([?\C-k] . [S-end delete])
             ([?\C-к] . [S-end delete])
             ([?\C-h] . [backspace])
             ([?\C-х] . [backspace])
             ([?\M-h] . [C-backspace])
             ([?\M-х] . [C-backspace])
             ([?\C-m] . [return])
             ([?\C-м] . [return])
             ([?\C-y] . [C-v])
             ([?\C-ы] . [C-v])
             ([?\M-w] . [C-c])
             ([?\M-в] . [C-c])))
  (:option exwm-input-prefix-keys
           (mapcar (lambda (k) (seq-elt (kbd k) 0))
                   '("C-x" "C-u" "C-?" "M-x" "M-&" "M-!" "M-:"
                     "M-<tab>" "M-S-<iso-lefttab>" "s-<tab>" "s-a" "s-e"
                     "s-t" "M-o" "s-n" "s-p" "s-b" "s-f" "s-h"
                     "s-0" "s-1" "s-2" "s-3" "C-s-b" "s-r"
                     "s-o" "s-k" "C-s-k")))
  (:option (append exwm-input-global-keys)
           (cons (kbd "s-&") #'my-start-program))
  (:option exwm-manage-configurations
           '((t
              floating-mode-line nil)))
  (:option exwm-workspace-number 1)
  (:with-hook exwm-update-class-hook
    (:hook (lambda ()
             (exwm-workspace-rename-buffer exwm-class-name))))
  (:with-hook exwm-update-title-hook
    (:hook (lambda ()
             (when (not exwm-instance-name)
               (exwm-workspace-rename-buffer exwm-title)))))
  (:with-hook after-init-hook
    (:hook exwm-enable)))

;;; Programming languages

;;;; Rust
(setup (:require rust-mode))

;;;; Raku
(setup raku-mode
  (:require raku-mode)
  (:option raku-indent-offset 8))

;;;; Shell
(setup executable
  (:with-hook after-save-hook
    (:hook executable-make-buffer-file-executable-if-script-p)))

;;;; Scheme
(setup scheme
  (:option scheme-program-name (executable-find "guile"))
  (:hook #'my-disable-local-tabs))

;;;;; Geiser
(setup geiser
  (:also-load geiser-guile geiser-autodoc)
  (:with-feature geiser-mode
    (:unbind "C-." "M-,"))
  (:with-map geiser-repl-mode-map
    (:unbind "C-a" "C-."))
  (:option geiser-default-implementation 'guile)
  (:option geiser-active-implementations '(guile))
  (:option geiser-repl-history-filename
           (expand-file-name ".geiser_history" user-emacs-directory))
  (:option geiser-repl-per-project-p t)
  (:option geiser-guile-binary (executable-find "guile"))
  (:option geiser-guile-load-init-file-p t)
  (:option geiser-autodoc-delay 0))

;;;; ELisp
(setup emacs-lisp
  (:option eval-expression-print-level nil)
  (:option eval-expression-print-length nil)
  (:hook #'my-disable-local-tabs)
  (:with-feature eros
    (:hook-into emacs-lisp-mode)))

;;;; Language agnostic

;;;;; Parentheses
(setup rainbow-delimiters
  (:hook-into prog-mode comint-mode))

(setup show-paren
  (:option show-paren-delay 0)
  (:hook-into prog-mode comint-mode))

(setup (:require parcont)
  (parcont-define-keys lisp-interaction-mode-map))

(setup puni
  (:bind [remap backward-delete-char] #'puni-backward-delete-char
         [remap backward-kill-word] #'puni-backward-kill-word
         [remap delete-char] #'puni-forward-delete-char
         [remap kill-word] #'puni-forward-kill-word
         [remap kill-line] #'puni-kill-line
         "M-r" #'puni-raise
         ;; puni-split is _very_ useful for strings
         "C-M-s" #'puni-split)
  (:unbind
   ;; They should use [remap ...] in definition of their `puni-mode-map'
   ;; TODO: report to upstream?
   ;; https://github.com/AmaiKinono/puni
   ;; BTW, how to unbind [remap ...] bindings?
   "C-d" "C-k" "M-d" "DEL"
   "M-)")
  (puni-global-mode))

(setup elec-pair
  (electric-pair-mode))

;;;;; REPLs
(setup comint
  (:option comint-highlight-input nil)
  (:option comint-prompt-read-only t)
  (:option comint-input-ignoredups t))

;;;;; Syntax checking
(setup flymake
  (:bind "M-n" #'flymake-goto-next-error
         "M-p" #'flymake-goto-prev-error)
  (:option flymake-mode-line-format
           '(" "
             flymake-mode-line-counters
             flymake-mode-line-exception))
  (:option next-error-message-highlight t)
  (:hook-into prog-mode))

;;;;; xref
(setup xref
  (:option xref-show-definitions-function
           #'xref-show-definitions-completing-read))

;;;;; Eldoc
(setup eldoc
  (:option eldoc-idle-delay 0)
  (global-eldoc-mode))

;;;;; Imenu
(setup imenu
  (:option imenu-auto-rescan t))

;;;;; prettify-symbols
(setup prettify-symbols
  (:option prettify-symbols-unprettify-at-point 'right-edge)
  (:option lisp-prettify-symbols-alist '(("lambda" . ?λ)
                                         ("iota"   . ?ι)
                                         ("."      . ?•)
                                         ("#f"     . ?𝔽)
                                         ("#t"     . ?𝕋)))
  (:hook-into prog-mode comint-mode))

;;;;; Project management
(setup project
  (:option project-switch-commands
           '((?f "Find file" project-find-file)
             (?g "Find regexp"
                 (lambda ()
                   (interactive)
                   (consult-grep (cdr (project-current t)))))
             (?d "Dired" project-dired)
             (?e "Eshell" project-eshell)
             (?v "VC Dir" project-vc-dir)
             (?m "Magit" magit-project-status)))
  (:option project-compilation-buffer-name-function
           #'project-prefixed-buffer-name))

(setup project-x
  (project-x-mode))

;;;;; outline-minor-mode
(setup outline
  (:with-mode outline-minor-mode
    (:bind "<tab>" #'outline-cycle)
    (:bind "C-c C-u" #'outline-up-heading)
    (:bind "C-c C-n" #'outline-next-visible-heading)
    (:bind "C-c C-p" #'outline-previous-visible-heading)
    (:bind "C-c C-f" #'outline-forward-same-level)
    (:bind "C-c C-b" #'outline-backward-same-level))
  (:with-map outline-navigation-repeat-map
    (:bind "<tab>" #'outline-cycle)
    (:unbind "C-u" "C-p" "C-n" "C-b" "C-f"))
  (put 'outline-cycle 'repeat-map 'outline-navigation-repeat-map)
  (:option outline-minor-mode-highlight 'append))

;;;;; compile
(setup compile
  (:with-map compilation-mode-map
    (:unbind "M-{" "M-}"))
  (:option compilation-max-output-line-length nil)
  (:require ansi-color)
  (:option ansi-color-for-compilation-mode t)
  (:with-hook compilation-filter-hook
    (:hook #'ansi-color-compilation-filter)))

;;;;;; compile-multi
(setup compile-multi
  (:global-into user-tools-map
    "C-c" #'compile-multi)
  (:option
   compile-multi-config
   `(((file-exists-p "Makefile")
      ,#'compile-multi-make-targets)
     (emacs-lisp-mode
      ("eval buffer" . ,#'eval-buffer))
     (scheme-mode
      ("guile:execute"
       "guile" file-name)
      ("guile:execute-opt"
       "guild" "compile" "-O3" file-name
       "&&"
       "guile" file-name)
      ("guix:build"
       "guix" "build"
       "-f" file-name))
     (skribilo-mode
      ("skribilo:html"
       "skribilo --target=html" file-name
       "-o"
       (concat
        (file-name-sans-extension
         (buffer-file-name))
        ".html"))
      ("skribilo:latex"
       "skribilo --target=latex" file-name
       "-o"
       (concat
        (file-name-sans-extension
         (buffer-file-name))
        ".tex")))
     (c-mode
      ("gcc:run"
       "gcc" "-Wall" file-name "&&" "./a.out")
      ("gcc:run-opt"
       "gcc" "-Wall" "-O3" file-name "&&" "./a.out"))
     (c++-mode
      ("g++:run"
       "g++" "-Wall" file-name "&&" "./a.out")
      ("g++:run-opt"
       "g++" "-Wall" "-O3" file-name "&&" "./a.out"))
     (rust-mode
      ("rustc:run"
       "rustc" file-name
       "&&"
       (file-name-sans-extension
        (buffer-file-name))))
     (raku-mode
      ("raku:run"
       "raku" file-name)
      ("raku:run-opt"
       "raku" "--optimize=3" file-name)))))

;;;;; hippie-expand
(setup hippie-exp
  (:global "M-/" #'hippie-expand)
  (:option hippie-expand-try-functions-list
           '(try-complete-file-name-partially
             try-complete-file-name
             try-expand-all-abbrevs
             try-expand-dabbrev
             try-expand-dabbrev-all-buffers
             try-expand-dabbrev-from-kill
             try-complete-lisp-symbol-partially
             try-complete-lisp-symbol)))

;;;;; cycle-at-point
(setup cycle-at-point
  ;; Someday I'll integrate this in my workflow
  (:option cycle-at-point-list '())
  (:global "s-/" #'cycle-at-point))

;;;;; Indentation
(setup indentation
  (:option tabify-regexp "^\t* [ \t]+")
  (defun my-indent-whole-buffer ()
    "Indent whole buffer according to local variables."
    (interactive)
    (unless (derived-mode-p 'makefile-mode
                            'fundamental-mode
                            'org-mode
                            'diary-mode
                            'diff-mode
                            'conf-unix-mode)
      (indent-region (point-min) (point-max)))
    ;; We still need to untabify wheh `indent-tabs-mode' is t to prevent
    ;; tabs in alignment (after code but before comment, etc.)
    (untabify (point-min) (point-max))
    (when indent-tabs-mode
      (tabify (point-min) (point-max)))
    (delete-trailing-whitespace))
  (:with-hook before-save-hook
    (:hook #'my-indent-whole-buffer)))

(setup cc-vars
  (:option c-basic-offset 8)
  (:option c-default-style '((awk-mode . "awk")
                             (other . "linux")))
  (:option c-cleanup-list '(brace-else-brace
                            brace-elseif-brace
                            empty-defun-braces
                            defun-close-semi
                            space-before-funcall
                            compact-empty-funcall))
  (:option c-hanging-braces-alist
           '((defun-open after)
             (substatement-open after)
             (brace-list-open after)
             (block-close . c-snug-do-while))))

(setup electric
  (electric-indent-mode))

;;;;;; aggressive-indent-mode
(setup aggressive-indent
  (:hook-into scheme-mode geiser-repl-mode emacs-lisp-mode))

;;;;;; Whitespaces highlighting
(setup whitespace
  (:option whitespace-style '(face trailing missing-newline-at-eof))
  (:hook-into prog-mode))

;;;;;; Fill column indicator
(setup display-fill-column-indicator
  (:hook-into prog-mode))

;;;;; Snippets

(setup tempel
  (defun my-current-buffer-prefix ()
    "Return sans filename or \"my\" if buffer not associated with file."
    (or (ignore-errors
          (file-name-base
           (buffer-file-name)))
        "my"))
  (:global "C-," #'tempel-expand)
  (:with-map tempel-map
    (:bind "M-a" #'tempel-previous
           "M-e" #'tempel-next
           "C-M-g" #'tempel-done)))

(setup (:require abbrev)
  (:hook-into prog-mode))

;;; Appearance
;;;; all-the-icons
(setup (:require all-the-icons))

;;;; Startup screen
(setup startup
  (:option inhibit-startup-screen t))

;;;; Themes
(setup themes
  (defvar after-load-theme-hook nil
    "Hook run after a color theme is loaded using `load-theme'.")
  (defadvice load-theme (after run-after-load-theme-hook activate)
    "Run `after-load-theme-hook'."
    (run-hooks 'after-load-theme-hook))
  (defun my-disable-themes-but-first ()
    (interactive)
    (mapc #'disable-theme
          (cdr-safe custom-enabled-themes)))
  (:with-hook after-load-theme-hook
    (:hook #'my-disable-themes-but-first))
  (:with-feature modus-themes
    (:require modus-themes)
    (:global-into user-tools-map
      "s-t" #'modus-themes-toggle)
    (:option modus-themes-deuteranopia t) ; Feels more consistent
    (:option modus-themes-prompts '(background bold))
    (:option modus-themes-syntax '())
    (:option modus-themes-lang-checkers '(intense text-also))
    (:option modus-themes-bold-constructs t)
    (:option modus-themes-italic-constructs t)
    (:option modus-themes-paren-match '(intense))
    (:option modus-themes-region '(bg-only accented no-extend))
    (:option modus-themes-mode-line '(accented))
    (:option modus-themes-tabs-accented t)
    (:option modus-themes-box-buttons '(flat accented))
    (:option modus-themes-links '(background italic))
    (:option modus-themes-completions
             '((matches . (background))
               (selection . (accented intense))))
    (:option modus-themes-no-mixed-fonts t)
    (:option modus-themes-headings '((t . (rainbow background))))
    (load-theme 'modus-operandi t))
  (:option x-underline-at-descent-line t)
  (:option indicate-empty-lines t)
  (:option text-scale-remap-header-line t)
  (:with-feature fonts
    (:option (append default-frame-alist) '(font . "mononoki-18"))
    (:face default        ((t (:font "mononoki-18"))))
    (:face fixed-pitch    ((t (:font "mononoki-18"))))
    (:face variable-pitch ((t (:font "mononoki-18"))))))

;;;; Modeline
(setup modeline
  (:with-feature time
    (:option display-time-format " %d.%m, %a, %H:%M")
    (:option display-time-default-load-average nil)
    (display-time))
  (:with-feature battery
    (:with-map user-tools-map
      (:bind "C-b" #'battery))
    (:option battery-mode-line-format "[%b%p]")
    (display-battery-mode))
  (:option mode-line-position '("%p of %I (%l:%c)"))
  (:option mode-line-format nil)
  (defvar my-info-line-format
    '("%e"
      mode-line-front-space
      mode-line-mule-info
      mode-line-client
      mode-line-modified
      mode-line-remote
      " "
      mode-line-buffer-identification
      " "
      mode-line-position
      " "
      minions-mode-line-modes
      (vc-mode vc-mode)
      global-mode-string
      mode-line-end-spaces)
    "Like a `mode-line-format' but used in tab-bar.")
  (defun my-tab-bar-info-line ()
    "Produce display of `my-info-line-format' in the tab bar."
    (format-mode-line my-info-line-format))
  (:option tab-bar-format
           '(tab-bar-format-menu-bar
             tab-bar-format-align-right
             my-tab-bar-info-line))
  (:option global-mode-string '((:propertize frct-mode-line-string
                                             face mode-line-emphasis)
                                " "
                                appt-mode-string
                                battery-mode-line-string
                                display-time-string))
  (:option mode-line-buffer-identification
           (propertized-buffer-identification "%b"))
  (:with-feature minions
    (:option minions-prominent-modes
             '(flymake-mode
               compilation-mode
               text-scale-mode
               defining-kbd-macro))
    (:option minions-mode-line-lighter "")
    (minions-mode)))

;;;; tooltips
(setup tooltip
  (tooltip-mode -1))

;;;; hl-line
(setup hl-line
  (:hook-into vc-annotate-mode ibuffer-mode package-menu-mode
              towel-mode))

;;;; window-divider
(setup frame
  (:option window-divider-default-places t)
  (window-divider-mode))

;;; Window and buffer management
(setup window
  (:global "s-0" #'delete-window
           "s-1" #'delete-other-windows
           "s-2" #'split-window-vertically
           "s-3" #'split-window-horizontally
           "C-s-b" #'balance-windows-area
           "s--" #'fit-window-to-buffer
           "s-o" #'mode-line-other-buffer
           "s-k" #'kill-this-buffer
           "C-s-k" #'kill-buffer-and-window)
  (:option display-buffer-alist
           '(("\\*Help\\*"
              (display-buffer-reuse-window
               display-buffer-in-direction)
              (window-width . 0.25)
              (direction . leftmost))
             ("\\*compilation\\*"
              (display-buffer-reuse-window
               display-buffer-in-direction)
              (window-width . 0.5)
              (direction . rightmost)
              (slot . 0))))
  (:option split-height-threshold nil)
  (:option split-width-threshold 80)
  (:option fit-window-to-buffer-horizontally t))

;;;; transpose-frame and window rotation
(setup transpose-frame
  (:global "s-r" #'rotate-frame-clockwise))

;;;; ace-window
(setup ace-window
  (:global "M-o" #'ace-window)
  (:option aw-keys '(?a ?o ?e ?u ?i ?d ?h ?t ?n ?s ?p ?y ?f ?g))
  (:option aw-scope 'frame))

;;;; windmove
(setup windmove
  (:global "s-n" #'windmove-down
           "s-p" #'windmove-up
           "s-b" #'windmove-left
           "s-f" #'windmove-right)
  (:option windmove-wrap-around t))

;;;; tab-bar
(setup tab-bar
  (:global "M-<tab>" #'tab-next
           "M-S-<iso-lefttab>" #'tab-previous
           "s-<tab>" #'toggle-frame-tab-bar)
  (:option tab-bar-close-button-show nil)
  (:option tab-bar-new-button-show nil)
  (tab-bar-history-mode)
  (unless (> (length (tab-bar-tabs)) 1)
    (tab-rename "1")
    (tab-new)
    (tab-rename "2")
    (tab-next)))

;;;; popper
(setup popper
  (:bind "s-h"   #'popper-toggle-latest
         "C-s-h" #'popper-toggle-type)
  (:option popper-reference-buffers
           '("^\\*eshell.*\\*$" eshell-mode
             "\\*Messages\\*"
             "\\*Async Shell Command\\*"
             "\\*Shell Command Output\\*"
             "\\*Help\\*" help-mode
             compilation-mode))
  (:option popper-mode-line nil)
  (:option popper-display-control nil)
  (:option popper-echo-dispatch-keys
           '("s-0" "s-1" "s-2" "s-3" "s-4" "s-5"))
  (popper-mode)
  (popper-echo-mode))

;;; Text editing
;;;; multiple-cursors
(setup multiple-cursors
  (:global "C->" #'mc/mark-next-like-this
           "C-<" #'mc/mark-previous-like-this)
  (:global-into ctl-x-map
    "C-n" #'mc/insert-numbers)
  (multiple-cursors-mode))

;;;; align
(setup (:require align)
  (:new-map my-align-map (kbd "s-t a")        ; "s-t a" is ugly
    "Keymap for `align' bindings.")
  (:with-map my-align-map
    (:bind "a" #'align
           "r" #'align-regexp))
  (:option align-to-tab-stop nil))

;;;; isearch
(setup isearch
  (:bind "C-h" #'isearch-delete-char)
  (:option search-whitespace-regexp "[ \t\n]+")
  (:option isearch-lazy-count t)
  (:option isearch-allow-scroll 'unlimited)
  (:option isearch-allow-motion t)
  (:option isearch-repeat-on-direction-change t))

;;;; avy
(setup avy
  (:global "M-m" #'avy-goto-word-0)
  (:option avy-keys '(?a ?o ?e ?u ?i ?d ?h ?n ?s ?p ?y ?f ?g))
  (:option avy-style 'at-full)
  ;; TODO: add `embark' support
  (:option avy-dispatch-alist '((?k . avy-action-kill-stay)
                                (?m . avy-action-mark))))

;;;; link-hint
(setup link-hint
  (defun my-link-hint-dwim (arg)
    "Open or copy url.

Act like `link-hint-open-link' without prefix ARG and like
`link-hint-copy-link' otherwise."
    (interactive "P")
    (if arg
        (link-hint-copy-link)
      (link-hint-open-link)))
  (:global "s-m" #'my-link-hint-dwim))

;;;; General
(setup text
  (:hook #'auto-fill-mode))

;;;; Pages
(setup page
  (:global "s-a" backward-page
           "s-e" forward-page)
  (put 'narrow-to-page 'disabled nil))

;;;;; form-feed
(setup form-feed
  (global-form-feed-mode))


;;; Minibuffer completion
(setup minibuffer
  (:with-map minibuffer-local-completion-map
    (:unbind "SPC" "?"))
  (:option minibuffer-completion-auto-choose nil)
  (:option completion-show-help nil)
  (minibuffer-depth-indicate-mode)
  (:option enable-recursive-minibuffers t)
  (:option history-delete-duplicates t)
  (savehist-mode)
  (:global "C-." #'completion-at-point)
  (:option completion-category-overrides
           '((file
              (styles . (basic partial-completion orderless)))
             (project-file
              (styles . (basic substring partial-completion orderless)))))
  (:option minibuffer-prompt-properties
           '(read-only t cursor-intangible t face minibuffer-prompt))
  (:option completions-format 'one-column)
  (:option completions-detailed t)
  (:option completions-group t)
  (:option completion-ignore-case t)
  (:option read-buffer-completion-ignore-case t)
  (:option read-file-name-completion-ignore-case t)
  (:require minibuf-eldef)
  (:option minibuffer-eldef-shorten-default t)
  (minibuffer-electric-default-mode)
  (:option resize-mini-windows t))

;;;; icomplete
(setup icomplete
  (:with-map icomplete-minibuffer-map
    (:bind "C-j" #'exit-minibuffer)
    (:bind "RET" #'icomplete-force-complete-and-exit)
    (:bind "M-RET" #'icomplete-force-complete))
  (:option icomplete-show-matches-on-no-input t)
  (:option icomplete-scroll t)
  (icomplete-vertical-mode))

;;;; orderless
(setup orderless
  (:require orderless)
  (:option orderless-matching-styles
           '(orderless-regexp
             orderless-prefixes
             orderless-literal))
  (:option orderless-style-dispatchers
           '((lambda (pattern _index _total)
               (cond
                ((string-suffix-p "," pattern)
                 `(orderless-initialism . ,(substring pattern 0 -1)))
                ((string-suffix-p "!" pattern)
                 `(orderless-without-literal . ,(substring pattern 0 -1)))
                ((string-suffix-p "=" pattern)
                 `(orderless-literal . ,(substring pattern 0 -1)))))))
  (:option completion-styles '(orderless)))

;;;; consult
(setup consult
  (:global [remap switch-to-buffer] #'consult-buffer
           [remap list-buffers] #'consult-buffer
           [remap project-switch-to-buffer] #'consult-project-buffer
           [remap yank-pop] #'consult-yank-pop
           [remap apropos-command] #'consult-apropos
           [remap goto-line] #'consult-goto-line
           [remap bookmark-jump] #'consult-bookmark)
  (:global-into goto-map
    "i" #'consult-imenu
    "o" #'consult-outline
    "s" #'consult-grep
    "e" #'consult-flymake
    "l" #'consult-line
    "r" #'consult-register)
  (:global-into user-tools-map
    "h" #'consult-history)
  (:option consult-narrow-key (kbd ">"))
  (:option completion-in-region-function
           #'consult-completion-in-region)
  (:with-hook completion-list-mode-hook
    (:hook #'consult-preview-at-point-mode)))

;;;; embark
(setup embark
  (:require embark-consult)
  (:global "M-," #'embark-act)
  (:option prefix-help-command #'embark-prefix-help-command)
  (:option embark-indicators '(embark-minimal-indicator
                               embark-highlight-indicator)))

;;; Dired
(setup dired
  (:also-load dired-x all-the-icons-dired dired-subtree)
  (:hook #'all-the-icons-dired-mode)
  (:option dired-omit-files (rx bol "."))
  ;; Main dired configuration
  (defun my-dired-rename-buffer ()
    "Rename current dired buffer to something more convenient."
    (rename-buffer (format "*DIRED: %s*"
                           (abbreviate-file-name default-directory))
                   t))
  (:global-into user-tools-map
    "d" #'dired
    "C-d" #'dired-jump)
  (:bind "b" #'dired-up-directory
         "TAB" #'dired-subtree-toggle
         "C-c C-u" #'dired-subtree-up
         "C-c C-p" #'dired-subtree-previous-sibling
         "C-c C-n" #'dired-subtree-next-sibling)
  (advice-add #'dired-subtree-toggle :after ; Show icons in subtrees
              (lambda (&rest _args) (revert-buffer))
              '((name . revert-buffer-after-subtree-toggle)))
  (:option dired-kill-when-opening-new-dired-buffer t)
  (:option dired-dwim-target t)
  (:option dired-auto-revert-buffer t)
  (:option dired-do-revert-buffer t)
  (:option dired-recursive-copies 'always)
  (:option dired-recursive-deletes 'always)
  (:option dired-guess-shell-alist-user
           `((,(rx "." (or "mp4" "webm" "mp3" "flac" "mkv" "cue")
                   string-end)
              "mpv --force-window")
             (,(rx "." (or "docx" "doc" "odt" "pptx" "ppt" "xlsx" "xls"
                           "pdf")
                   string-end)
              "libreoffice")))
  (:option dired-listing-switches "-AGFhlv")
  (:hook #'my-dired-rename-buffer #'dired-hide-details-mode #'hl-line-mode))

;;; Eshell
(setup eshell
  (defun my-eshell-clear ()
    "Clear current eshell buffer."
    (interactive)
    (eshell/clear-scrollback)
    (eshell-send-input))
  (defun my-eshell-input-filter (str)
    "Return nil if STR should not be found in eshell history."
    (not (or (string= "" str)
             (string= "cd" str)
             (string-prefix-p "cd " str)
             (string-prefix-p "~" str)
             (string-prefix-p "/" str)
             (string-prefix-p "." str)
             (string-prefix-p " " str))))
  (:global-into user-tools-map
    "e" #'eshell)
  (:with-feature esh-mode
    (:with-map eshell-mode-map
      (:bind "C-c C-o" #'my-eshell-clear)))
  (:with-feature em-hist
    (:with-map eshell-hist-mode-map
      (:unbind "M-s" "M-r")))
  (:option eshell-prefer-lisp-functions nil) ; Avoid *guix and *notmuch
  (:option eshell-hist-ignoredups 'erase)
  (:option eshell-input-filter #'my-eshell-input-filter)
  (:option eshell-prompt-function
           (lambda ()
             (concat
              (abbreviate-file-name (eshell/pwd)) " "
              (if (zerop eshell-last-command-status)
                  ""
                (concat (number-to-string eshell-last-command-status)
                        " "))
              (if (zerop (user-uid)) "#" "$")
              " ")))
  (:option eshell-prompt-regexp
           (rx bol
               (zero-or-more (not (any "#" "$" "\n"))) " "
               (zero-or-one (one-or-more num) " ")
               (or "#" "$") " "))
  (setenv "PAGER" "cat")
  (:with-hook eshell-preoutput-filter-functions
    (:hook #'ansi-color-apply))
  (defun my--eshell-buffer-names ()
    "Return list of buffers with eshell-mode."
    (mapcar
     #'buffer-name
     (seq-filter
      (lambda (buf)
        (provided-mode-derived-p
         (buffer-local-value 'major-mode buf)
         'eshell-mode))
      (buffer-list))))
  (defvar my--eshell-buffer-source
    (list :name "Emacs shell"
          :hidden t
          :narrow ?e
          :category 'buffer
          :state #'consult--buffer-state
          :items #'my--eshell-buffer-names))
  (:option (append consult-buffer-sources)
           #'my--eshell-buffer-source))

;;; Tools

;;;; epa
(setup epa
  (:option epa-file-encrypt-to "EB90668A3B093765"))

;;;; remember
(setup remember
  (:global-into user-tools-map
    "n" #'remember-notes)
  (:option remember-annotation-functions
           (list (lambda () (concat "Buffer: " (buffer-name)))))
  (:option remember-time-format "")
  (:option remember-notes-initial-major-mode 'fundamental-mode)
  (:option remember-notes-bury-on-kill nil)
  (:option remember-data-file (expand-file-name
                               "notes.gpg"
                               user-emacs-directory)))

;;;; howm
;; See: https://leahneukirchen.org/blog/archive/2022/03/note-taking-in-emacs-with-howm.html
(setup (:require howm)
  (:with-map howm-view-summary-mode-map
    (:unbind "M-<tab>")
    (:bind "S-SPC" #'scroll-other-window-down))
  (:with-map howm-menu-mode-map
    (:unbind "M-<tab>")
    (:bind "<backtab>" #'action-lock-goto-previous-link))
  (:option howm-directory "~/dcmnts/howm/")
  (:option howm-keyword-file
           (expand-file-name ".howm-keys" howm-directory))
  (:option howm-history-file
           (expand-file-name ".howm-history" howm-directory))
  (:hook howm-mode-set-buffer-name))

;;;; Diary, appt, calendar
(setup (:require diary-lib)
  (:option diary-comment-start ";;")
  (:with-feature calendar
    (:global-into user-tools-map "M-c" #'calendar)
    (:bind "n" #'calendar-forward-week
           "p" #'calendar-backward-week
           "b" #'calendar-backward-day
           "f" #'calendar-forward-day)
    (:with-hook calendar-today-visible-hook
      (:hook #'calendar-mark-today))
    (calendar-set-date-style 'iso)
    (:option calendar-week-start-day 1))
  (:with-feature appt
    (:option appt-display-format 'echo)
    (:option appt-message-warning-time 45)
    (appt-activate 1)))

;;;; org
(setup org
  (:unbind "M-h")
  (:option org-adapt-indentation t))

;;;; calc
(setup (:require calc)
  (:global-into user-tools-map
    "c" #'calc
    "q" #'quick-calc)
  (:option calc-show-banner nil)
  (:option calc-group-char "_")
  (:option calc-display-trail nil)
  (:option calc-make-windows-dedicated t)
  (:option calc-algebraic-mode t)
  (:option calc-angle-mode 'rad)
  (:option calc-symbolic-mode t)
  ;; Nice in combination with scratch.el
  (:require literate-calc-mode))

;;;; proced
(setup (:require proced)
  (:option proced-auto-update-flag t)
  (:option proced-auto-update-interval 1)
  (:option proced-filter 'all))

;;;; pdf-tools
(setup (:require pdf-tools)
  (:require saveplace-pdf-view)
  (:option pdf-view-display-size 'fit-page)
  (pdf-tools-install))

;;;; nov
(setup (:require nov)
  (:file-match "\\.epub\\'")
  (:option nov-text-width 72))

;;;; olivetti
(setup (:require olivetti)
  (:global-into user-tools-map
    "o" #'olivetti-mode)
  (:option olivetti-body-width 72)
  (:option olivetti-style 'fancy)
  (:hook-into Info-mode nov-mode org-mode))

;;;; desktop-environment
(setup (:require desktop-environment)
  (:option desktop-environment-screenshot-command
           "flameshot gui -p /tmp")
  (:option desktop-environment-screenshot-directory "/tmp")
  (desktop-environment-mode))

;;;; Guix
(setup guix
  (:global-into user-tools-map
    "M-g" #'guix))

;;;; ERC
(setup erc
  (:global-into user-tools-map
    "i" #'erc-tls)
  (:option erc-server "irc.libera.chat")
  (:option erc-nick "yosik")
  (:option erc-full-name "Mihail Iosilevitch")
  (:option erc-use-auth-source-for-nickserv-password t)
  (:option erc-hide-list '("JOIN" "PART" "QUIT"))
  (:option erc-fill-column 72)
  (:option erc-fill-function #'erc-fill-static
           erc-fill-static-center 15)
  (:option erc-insert-timestamp-function
           #'erc-insert-timestamp-left
           erc-insert-away-timestamp-function
           #'erc-insert-timestamp-left)
  ;; Show updates in modeline
  (:option erc-track-position-in-mode-line t
           ;; erc-track-when-inactive t
           erc-track-shorten-cutoff 15
           (prepend erc-modules) 'track)
  (:global "C-c C-s" #'erc-track-switch-buffer)
  ;; Misc modules
  (:option (prepend erc-modules) 'keep-place
           (prepend erc-modules) 'notifications)
  (erc-update-modules)
  ;; Filter consult buffers
  (defvar my--irc-buffer-source
    (list :name "IRC"
          :hidden t
          :narrow ?i
          :category 'buffer
          :state #'consult--buffer-state
          :items #'erc-all-buffer-names))
  (:option (append consult-buffer-sources)
           #'my--irc-buffer-source))

;;;; telega.el
(setup (:require telega)
  (:global-into user-tools-map
    "t" #'telega)
  (:with-map telega-msg-button-map
    (:bind "q" #'bury-buffer
           "SPC" #'scroll-up-command
           "S-SPC" #'scroll-down-command))
  (:option telega-server-libs-prefix "/usr")
  (:option telega-chat-ret-always-sends-message nil)
  (:option telega-chat-input-markups '("org" "markdown2" nil))
  (:option telega-video-player-command "mpv")
  (:option telega-completing-read-function 'completing-read)
  (:option telega-online-status-function #'telega-buffer-p)
  (:face telega-entity-type-code ((t (:inherit org-code))))
  (:face telega-entity-type-pre  ((t (:inherit org-block)))))

;;;; ement
(setup (:require ement)
  (:option ement-save-sessions t)
  (:option ement-room-send-message-filter #'ement-room-send-org-filter)
  (:option ement-room-message-format-spec ;; IRC without margins
           "%S> %B%r")
  (:with-feature ement-taxy
    (:unbind "M-<tab>")))

;;;; EMail
;;;;; notmuch
(setup (:require notmuch)
  (defun my-update-mail ()
    "Run shell commands to update mail asynchronously."
    (interactive)
    (async-shell-command "mbsync --all && notmuch new"))
  (defun my-notmuch-hello-insert-update-mail-button ()
    "Insert button which updates mail."
    (widget-create 'link
                   :notify (lambda (&rest _ignore)
                             (my-update-mail))
                   :help-echo "Fetch new mail and store it."
                   "Update mail")
    (widget-insert "\n"))
  (:global-into user-tools-map "M-m" #'my-update-mail)
  (:option notmuch-hello-sections
           '(notmuch-hello-insert-header
             my-notmuch-hello-insert-update-mail-button
             notmuch-hello-insert-saved-searches
             notmuch-hello-insert-search
             notmuch-hello-insert-alltags
             notmuch-hello-insert-footer))
  (:global-into user-tools-map "m" #'notmuch)
  (:option notmuch-search-oldest-first nil)
  (:option notmuch-show-logo nil)
  (:option notmuch-saved-searches
           '((:name "todo" :query "tag:inbox OR tag:unread" :key "t")
             (:name "flagged" :query "tag:flagged" :key "f")
             (:name "sent" :query "tag:sent" :key "s")
             (:name "all mail" :query "*" :key "a")))
  (:option notmuch-wash-citation-lines-prefix 0)
  (:option notmuch-wash-citation-lines-suffix 0)
  (:option notmuch-always-prompt-for-sender t))

;;;;; sendmail and message
(setup sendmail
  (:option sendmail-program "msmtp")
  (:option message-sendmail-f-is-evil t)
  (:option message-send-mail-function #'message-send-mail-with-sendmail)
  (:option message-sendmail-extra-arguments '("--read-envelope-from"))
  (:option mail-envelope-from 'header)
  (:option message-signature "\n\n-- \nMihail Iosilevitch\n")
  (:option message-kill-buffer-on-exit t))

;;;; Web: browse-url, eww, shr
(setup browse-url
  (:option browse-url-handlers
           `((,(rx "git" (or "hub" "lab") ".com") . browse-url-firefox)
             (,(rx ".pdf" eol) . browse-url-firefox)))
  (:option browse-url-browser-function #'eww-browse-url)
  (:option browse-url-firefox-new-window-is-tab t)
  (:option url-privacy-level '(email lastloc cookies))
  (:option url-cookie-file nil))

(setup eww
  (defun my-eww-buffer-name ()
    "Return new eww buffer name."
    (when (eq major-mode 'eww-mode)
      (let ((string
             (if (string-empty-p
                  (plist-get eww-data :title))
                 (plist-get eww-data :url)
               (plist-get eww-data :title))))
        (format "*EWW: %s*" string))))
  (defun my-eww-fix-org-link (url)
    "Fix non-standard org-style protocol prefix in URL.

For example, with input like \"https://https:gnu.org\" returned
value will be \"https://gnu.org\"."
    (if (string-match
         (rx "://" (group (one-or-more letter) ":") (= 2 (not "/")))
         url)
        (replace-match "" nil nil url 1)
      url))
  (:global-into user-tools-map "b" #'eww)
  (:option eww-auto-rename-buffer #'my-eww-buffer-name)
  (:option eww-browse-url-new-window-is-tab nil)
  (:hook #'olivetti-mode)
  (:with-hook eww-url-transformers
    (:hook #'my-eww-fix-org-link)))

(setup shr
  (:option shr-cookie-policy nil)
  (:option shr-use-colors nil)
  (:option shr-use-fonts nil)
  (:option shr-image-animate nil)
  (:option shr-discard-aria-hidden t)
  (:option shr-width 72)
  (:option shr-bullet "• ")
  (:face shr-text ((t (:inherit fixed-pitch))))
  (:with-feature shr-tag-pre-highlight
    (:require shr-tag-pre-highlight)
    (:option (append shr-external-rendering-functions)
             '(pre . shr-tag-pre-highlight))))

;; TODO: Move in separate file?
(setup video
  (defvar my-mpv-history nil
    "History for `my-open-video-in-mpv' command.")
  (defun my-open-video-in-mpv (url)
    "Open URL in mpv asyncronously."
    (interactive
     (list (let ((uris
                  (seq-uniq
                   (seq-filter #'identity ; Remove nil
                               (list (ffap-url-at-point)
                                     (ffap-url-p
                                      (gui-get-selection 'PRIMARY))
                                     (ffap-url-p
                                      (car kill-ring)))))))
             (read-string (format-prompt "Enter URL"
                                         (and uris (car uris)))
                          nil 'my-mpv-history uris))))
    (start-process "mpv" nil "mpv" "--force-window" (url-encode-url url)))
  (:global-into user-tools-map
    "v" #'my-open-video-in-mpv))

;;;; 0x0
(setup 0x0
  (:require 0x0)
  (:global-into user-tools-map
    "C-p" #'0x0-dwim))

;;;; frct
(setup (:require frct)
  (:global-into user-tools-map
    "s-s" #'frct-switch))

;;;; towel
(setup (:require towel)
  (:require towel-my)
  (:global-into user-tools-map
    "w" #'towel-show))

;;;; midnight
(setup midnight
  ;; 03:00
  (:option midnight-delay (* 3 60 60))
  (midnight-mode))

;;;; password-store
(setup (:require password-store)
  (:new-map password-store-map (kbd "s-t p")
    "Keymap for `password-store' bindings."
    "c" #'password-store-copy
    "i" #'password-store-insert
    "g" #'password-store-generate)
  (:option auth-sources '(password-store)))

;;;; pinentry
(setup (:require pinentry)
  (:option epg-pinentry-mode 'loopback)
  ;; (pinentry-start)
  )

;;;; Git
;;;;; diff-mode
(setup diff-mode
  (:unbind "M-o"))

;;;;; vc
(setup vc-git
  (:with-feature vc-dir
    (:bind "d" #'vc-diff))
  (:global-into ctl-x-map
    "C-v" vc-prefix-map)
  (:option vc-revert-show-diff 'kill)
  (:with-feature vc-git
    (:with-map vc-git-log-edit-mode-map
      (:bind "C-c C-a" #'vc-git-log-edit-toggle-amend))
    ;; Based on Protesilaos Stavrou's dotemacs
    (defun my-vc-git-expanded-log-entry (revision)
      (with-temp-buffer
        (apply #'vc-git-command t nil nil
               (list "log" revision "-1"
                     "--stat"
                     "--decorate"
                     ;; "--show-signature"
                     "--color"
                     "--"))
        (goto-char (point-min))
        (unless (eobp)
          ;; Indent the expanded log entry.
          (while (re-search-forward "^" nil t)
            (replace-match "  ")
            (forward-line))
          (ansi-color-filter-apply
           (concat "\n" (buffer-string))))))
    (:with-hook vc-git-log-view-mode-hook
      (:hook (lambda ()
               (setq log-view-expanded-log-entry-function
                     #'my-vc-git-expanded-log-entry))
             #'hl-line-mode))
    (:option log-view-expanded-log-entry-function
             #'my-vc-git-expanded-log-entry)
    (:option vc-git-print-log-follow t)
    (:option vc-git-root-log-format
             `("%d %h %ad %G? %an: %s"
               ,(rx bol
                    (1+ (any "*" "/" "\\" "|"))
                    (? (1+ (any "*" "\\" "/" "|" " ")))
                    (? (group-n 2 "(" (1+ (not ")")) ") "))
                    (group-n 1 (1+ (any (?a . ?z) (?0 . ?9))))
                    " " (group-n 4
                                 (= 4 (any (?0 . ?9))) "-"
                                 (= 2 (any (?0 . ?9))) "-"
                                 (= 2 (any (?0 . ?9))))
                    " " (group-n 3 (0+ (not ":")))
                    ":")
               ((1 'log-view-message)
                (2 'change-log-list nil lax)
                (3 'change-log-name)
                (4 'change-log-date))))))

;;;;; magit
(setup (:require magit)
  (:global "s-g" #'magit-status)
  (:global-into user-tools-map
    "g" #'magit-status)
  (:unbind "M-<tab>")
  (:with-feature transient
    (:require transient)
    (:option transient-default-level 5)))

;;;; reverse-im
(setup (:require reverse-im)
  (load-file (expand-file-name
              "cyrillic-dvorak.el"
              user-emacs-directory))
  (:option reverse-im-input-methods '("cyrillic-dvorak"))
  (:option reverse-im-char-fold t)
  (reverse-im-mode)
  (:with-feature char-fold
    (:option char-fold-symmetric t)
    (:option search-default-mode #'char-fold-to-regexp)))


;;; Make gc pauses faster by decreasing the threshold.
(setq gc-cons-threshold (* 2 1000 1000))

;;; Metadata

;; Local Variables:
;; indent-tabs-mode: nil
;; byte-compile-warnings: (not unresolved free-vars)
;; eval: (outline-minor-mode)
;; End:

(provide 'init)
;;; init.el ends here
